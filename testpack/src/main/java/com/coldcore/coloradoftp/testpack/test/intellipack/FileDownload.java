package com.coldcore.coloradoftp.testpack.test.intellipack;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * @see com.coldcore.coloradoftp.testpack.test.genericbundle.FileDownload
 */
public class FileDownload extends com.coldcore.coloradoftp.testpack.test.genericbundle.FileDownload {

  private static Logger log = Logger.getLogger(FileDownload.class);


  protected void setUp() throws Exception {
    super.setUp("intellipack-beans.xml");
  }

}