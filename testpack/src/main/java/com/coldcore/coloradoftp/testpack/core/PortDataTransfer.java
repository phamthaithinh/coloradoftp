package com.coldcore.coloradoftp.testpack.core;

import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Reads/sends data from/to FTP server by using PORT mode.
 */
public class PortDataTransfer {

  private static Logger log = Logger.getLogger(PortDataTransfer.class);
  private ServerSocket ssc;
  private Socket sc;
  private InputStream sin;
  private OutputStream sou;
  private int port;
  private long time;
  private long datatime;
  private boolean busy;


  /**
   * @param port Local data port
   */
  public PortDataTransfer(int port) {
    this.port = port;
  }


  /**
   * Read data from a server
   * @return Server's data
   */
  synchronized public byte[] readData() throws Exception {
    openConnection();
    while (busy)
      Thread.sleep(100);
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    int i;
    byte[] b = new byte[4096];
    busy = true;
    long start = System.currentTimeMillis();
    long datastart = 0;
    while ((i = sin.read(b)) != -1) {
      if (datastart == 0) datastart = System.currentTimeMillis();
      bout.write(b, 0, i);
      Thread.yield();
    }
    datatime = System.currentTimeMillis()-datastart;
    time = System.currentTimeMillis()-start;
    busy = false;
    closeConnection();
    return bout.toByteArray();
  }


  /**
   * Read data from a server
   * @return Server's data as a string
   */
  synchronized public String readDataAsString() throws Exception {
    return new String(readData(), "UTF-8");

  }


  /**
   * Send data to a server
   * @param data Data to send
   */
  synchronized public void writeData(byte[] data) throws Exception {
    openConnection();
    while (busy)
      Thread.sleep(100);
    InputStream in = new ByteArrayInputStream(data);
    int i;
    byte[] b = new byte[4096];
    busy = true;
    long start = System.currentTimeMillis();
    long datastart = 0;
    while ((i = in.read(b)) != -1) {
      if (datastart == 0) datastart = System.currentTimeMillis();
      sou.write(b, 0, i);
      Thread.yield();
    }
    datatime = System.currentTimeMillis()-datastart;
    time = System.currentTimeMillis()-start;
    busy = false;
    closeConnection();
  }


  /**
   * Send a string to a server
   * @param data String to send
   */
  synchronized public void writeDataAsString(String data) throws Exception {
    writeData(data.getBytes("UTF-8"));
  }


  /** Connect to a server */
  synchronized public void openConnection() throws Exception {
    if (ssc != null || sc != null) return;
    busy = true;
    Thread thr = new Thread(new Runnable() {
      public void run() {
        try {
          ssc = new ServerSocket(port);
          sc = ssc.accept();
          ssc.close();
          sin = sc.getInputStream();
          sou = sc.getOutputStream();
          Thread.sleep(100);
          busy = false;
          log.info("Data transfer connected");
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
    });
    thr.start();
    Thread.sleep(100);
  }


  /** Disconnect from a server */
  synchronized public void closeConnection() {
    try {
      sou.close();
    } catch (Throwable e) {}
    try {
      sin.close();
    } catch (Throwable e) {}
    try {
      sc.close();
    } catch (Throwable e) {}

    ssc = null;
    sc = null;
    log.info("Data transfer disconnected");

    try {
      Thread.sleep(100);
    } catch (Throwable e) {}
  }


  /**
   * Get overall time spent during the last data transfer
   * @return Time in mills
   */
  public long getTime() {
    return time;
  }


  /**
   * Get precise time spent during the last data transfer (from the moment data actually started to flow)
   * @return Time in mills
   */
  public long getDatatime() {
    return datatime;
  }


  /** Test if this class is busy transferring data */
  public boolean isBusy() {
    return busy;
  }


  /** Test if connection is still open */
  public boolean isConnected() {
    return sc != null || ssc != null;
  }
}
