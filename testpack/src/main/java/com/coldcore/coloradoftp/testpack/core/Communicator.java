package com.coldcore.coloradoftp.testpack.core;

import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * FTP client providing low-level FTP communications.
 */
public class Communicator {

  private static Logger log = Logger.getLogger(Communicator.class);
  private Socket sc;
  private InputStream sin;
  private OutputStream sou;
  private String ip;
  private int port;
  private boolean logged;


  /**
   * @param ip Server IP
   * @param port Server port
   */
  public Communicator(String ip, int port) {
    this.ip = ip;
    this.port = port;
    logged = true;
  }


  public void setLogged(boolean logged) {
    this.logged = logged;
  }


  /**
   * Block and read the next server's reply
   * @return Server's reply
   */
  synchronized public String nextReply() throws Exception {
    StringBuffer sb = new StringBuffer();
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    boolean more = false;
    String code = "";
    int i;
    while ((i = sin.read()) != -1) {
      bout.write(i);
      //System.out.print(i);

      if (i == 10) {
        String line = bout.toString("UTF-8");
        bout.reset();

        sb.append(line);
        //System.out.println(" = " + line);

        if (code.equals("")) {
          code = line.substring(0,3);
          if (line.charAt(3) == '-') more = true;
        } if (line.length() > 3) {
          String code2 = line.substring(0,4);
          if ((code+" ").equals(code2)) more = false;
        }

        if (!more) break;
      }
    }

    Thread.sleep(100);

    if (i == -1) closeConnection();

    return sb.toString();
  }


  /**
   * Send message to a server
   * @param msg Message
   */
  synchronized public void send(String msg) throws Exception {
    sou.write((msg+"\r\n").getBytes("UTF-8"));
    Thread.sleep(100);
  }


  /** Connect to a server */
  synchronized public void openConnection() throws Exception {
    if (sc == null) {
      sc = new Socket(ip, port);
      sin = sc.getInputStream();
      sou = sc.getOutputStream();
    }

    if (logged) log.info("Connected to remote server");
    Thread.sleep(100);
  }


  /** Disconnect from a server */
  synchronized public void closeConnection() {
    try {
      sou.close();
    } catch (Throwable e) {}
    try {
      sin.close();
    } catch (Throwable e) {}
    try {
      sc.close();
    } catch (Throwable e) {}

    sc = null;
    if (logged) log.info("Remote server disconnected");

    try {
      Thread.sleep(100);
    } catch (Throwable e) {}
  }


  /**
   * Test if connection is still open
   * @return TRUE if open, FALSE is closed
   */
  public boolean isOpen() {
    return sc != null;
  }
}
