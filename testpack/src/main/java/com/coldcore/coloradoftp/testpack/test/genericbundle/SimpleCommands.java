package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test replies of the most simple commands.
 */
public class SimpleCommands extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(SimpleCommands.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testCompleteSimple() throws Exception {
    command("crazy", "502");
    command("allo", "202");
    command("help", "214");
    command("mode", "530");
    command("stru", "530");
    command("type", "530");
    command("noop", "200");
    command("syst", "215");
    command("opts noop", "504");

    anonymousLogin();

    command("allo", "202");

    command("help", "214");
    command("help ", "214");
    command("help feat", "211");
    command("help crazy", "504");

    command("mode", "501");
    command("mode ", "501");
    command("mode S", "200");
    command("mode crazy", "504");

    command("stru", "501");
    command("stru ", "501");
    command("stru F", "200");
    command("stru crazy", "504");

    command("type", "501");
    command("type ", "501");
    command("type I", "200");
    command("type A", "200");
    command("type crazy", "504");

    command("noop", "200");

    command("syst", "215");

    command("opts", "501");
    command("opts noop", "504");

    logout();
  }


  public void testUserPassQuit() throws Exception {
    command("quit", "530");

    command("pass crazy", "503");
    command("user", "501");

    command("user crazy", "331");
    command("pass", "501");

    command("user crazy", "331");
    command("pass crazy", "530");

    command("user anonymous", "331");
    command("pass ano@nym.ous", "230");

    command("user crazy", "503");
    command("pass crazy", "503");

    command("quit", "221");

    communicator.nextReply();
    if (communicator.isOpen()) fail("Connection open after QUIT");
  }


  public void testAbor() throws Exception {
    command("abor", "530");

    anonymousLogin();

    command("abor", "226");

    createRemoteTextFile(MBx1/10, remoteUsersPath+"/anonymous/testfile.txt", false);

    pasv();
    command("retr testfile.txt", "150");
    pasvTransfer.openConnection();
    communicator.send("abor");
    nextReplyCode("426");
    nextReplyCode("226");
    if (pasvTransfer.readDataHollow() == MBx1/10) fail("Data connection left open");

    logout();
  }


  public void testAnonymousLogin() throws Exception {
    anonymousLogin();
    logout();
  }


  public void testStat() throws Exception {
    command("stat", "530");

    anonymousLogin();

    command("stat", "211");

    createRemoteTextFile(MBx1/10, remoteUsersPath+"/anonymous/testfile.txt", false);

    pasv();
    command("retr testfile.txt", "150");
    pasvTransfer.openConnection();
    communicator.send("stat");
    nextReplyCode("221");
    pasvTransfer.readDataHollow();
    dataTransferComplete();

    //STAT with 212 code is tested elsewhere

    logout();
  }


  public void testQuit() throws Exception {
    command("quit", "530");

    anonymousLogin();

    Thread.sleep(500);
    command("quit", "221");
    Thread.sleep(500);

    communicator.nextReply();
    if (communicator.isOpen()) fail("Connection left open");
    log.info("Connection closed OK");

    communicator.nextReply();
    communicator.openConnection();
    communicator.nextReply();
    anonymousLogin();

    pasv();
    command("stor testfile-1.txt", "150");
    pasvTransfer.openConnection();
    Thread.sleep(500);
    command("quit", "221");
    Thread.sleep(500);

    if (!pasvTransfer.isConnected()) fail("Data connection closed");
    pasvTransfer.writeDataAsString("crazy data");
    log.info("Data connection OK");
    dataTransferComplete();

    communicator.nextReply();
    if (communicator.isOpen()) fail("Connection left open");
    log.info("Connection closed OK");

  }


  public void testInterrupt() throws Exception {
    anonymousLogin();

    command("noop", "200");
    command("syst", "215");

    pasv();
    command("stor testfile-1.txt", "150");
    pasvTransfer.openConnection();

    communicator.send("noop");
    communicator.send("crazy");
    communicator.send("syst");

    command("stat", "221");

    communicator.send("syst");
    communicator.send("noop");

    command("stat", "221");

    communicator.send("abor");
    nextReplyCode("426");
    nextReplyCode("226");

    command("noop", "200");
    command("syst", "215");

    log.info("Interrupt set/clear OK");

    logout();
  }

}
