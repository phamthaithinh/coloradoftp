package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test behaviour of REST command.
 */
public class ResumeFileDownload extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(ResumeFileDownload.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testRestRetr() throws Exception {
    command("rest", "530");

    anonymousLogin();

    createRemoteTextFile(256*2, remoteUsersPath+"/anonymous/testfile-1.txt", false);

    command("rest", "501");
    command("rest 0", "350");
    command("rest 256", "550");
    switchToBinaryMode();
    command("rest 256", "350");
    switchToAsciiMode();
    command("rest 256", "550");

    switchToBinaryMode();
    pasv();
    command("rest 0", "350");
    command("retr testfile-1.txt", "150");
    byte[] data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", data);

    byte[] half = new byte[256];
    System.arraycopy(data, 0, half, 0, half.length);

    pasv();
    command("rest 256", "350");
    command("retr testfile-1.txt", "150");
    data = pasvTransfer.readData();
    dataTransferComplete();

    byte[] full = new byte[half.length+data.length];
    System.arraycopy(half, 0, full, 0, half.length);
    System.arraycopy(data, 0, full, half.length, data.length);
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", full);

    logout();
  }

}
