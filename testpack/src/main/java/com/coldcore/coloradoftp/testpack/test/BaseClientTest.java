package com.coldcore.coloradoftp.testpack.test;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

/**
 * This is a base for test files. This class uses Apache FTP client.
 */
abstract public class BaseClientTest extends BaseNoClientTest {

  private static Logger log = Logger.getLogger(BaseClientTest.class);
  protected FTPClient ftpClient;


  protected void setUp() throws Exception {
    initializeFtpServer();
    startFtpServer();
    Thread.sleep(100);
    createRemoteContent();
    initializeFtpClient();
    connectFtpClient();
    Thread.sleep(100);
  }


  protected void tearDown() throws Exception {
    disconnectFtpClient();
    stopFtpServer();
    Thread.sleep(100);
  }


  /** Initialize FTP client */
  protected void initializeFtpClient() {
    if (ftpClient == null) {
      ftpClient = new FTPClient();
    }
  }


  /** Open connection to a server */
  protected void connectFtpClient() throws Exception {
    if (ftpClient == null) throw new Exception("No client");
    ftpClient.connect(remoteHost, remotePort);
    log.info("Remote server accepted connection");
  }


  /** Close connection to a server */
  protected void disconnectFtpClient() throws Exception {
    if (ftpClient == null) throw new Exception("No client");
    ftpClient.disconnect();
  }


  /** This login method should be used only in the setUp() */
  protected void loginFtpClient() throws Exception {
    if (ftpClient == null) throw new Exception("No client");
    if (!ftpClient.login(username, password)) throw new Exception("Login failed");
  }

}
