package com.coldcore.coloradoftp.testpack.test.intellipack;

import org.apache.log4j.Logger;
import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;

/**
 * @see com.coldcore.coloradoftp.testpack.test.genericbundle.BinaryFlow
 */
public class BinaryFlow extends com.coldcore.coloradoftp.testpack.test.genericbundle.BinaryFlow {

  private static Logger log = Logger.getLogger(BinaryFlow.class);


  protected void setUp() throws Exception {
    super.setUp("intellipack-beans.xml");
  }

}