package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test PORT/PASV commands.
 */
public class PortPasv extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(PortPasv.class);


  protected void setUp() throws Exception {
    this.setUp("genericbundle-beans.xml");
  }


  protected void setUp(String beansFilename) throws Exception {
    this.beansFilename = beansFilename;
    super.setUp();
  }


  public void testPasvBasics() throws Exception {
    command("pasv", "530");

    anonymousLogin();

    String reply;

    command("mkd testdir-1", "257", "250");

    pasv();
    command("nlst", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testdir-1")) fail("Listing NO: "+reply);

    command("nlst", "425");

    pasv();
    command("nlst", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testdir-1")) fail("Listing NO: "+reply);

    if (pasvTransfer.isConnected()) fail("Data connection left open");
    System.out.println("Data connection closed OK");
  }


  public void testPortBasics() throws Exception {
    command("port", "530");

    anonymousLogin();

    String reply;

    command("mkd testdir-1", "257", "250");

    command("port", "501");
    command("port crazy", "501");

    port();
    command("nlst", "150");
    reply = portTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testdir-1")) fail("Listing NO: "+reply);

    command("nlst", "150");
    reply = portTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testdir-1")) fail("Listing NO: "+reply);

    port();
    command("nlst", "150");
    reply = portTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testdir-1")) fail("Listing NO: "+reply);

    if (portTransfer.isConnected()) fail("Data connection left open");
    System.out.println("Data connection closed OK");
  }


  public void testReadWriteList() throws Exception {
    anonymousLogin();

    createRemoteBinaryFile(MBx1, remoteUsersPath+"/anonymous/testfile-1mb.dat");

    ensureListedInWorkingDirectory("testfile-1mb.dat");

    byte[] bdata;

    switchToBinaryMode();

    pasv();
    command("retr testfile-1mb.dat", "150");
    bdata = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1mb.dat", bdata);

    port();
    command("retr testfile-1mb.dat", "150");
    bdata = portTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1mb.dat", bdata);

    pasv();
    command("stor testfile-1mb-2.dat", "150");
    pasvTransfer.writeData(bdata);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1mb-2.dat", bdata);

    port();
    command("stor testfile-1mb-3.dat", "150");
    portTransfer.writeData(bdata);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1mb-3.dat", bdata);

    ensureListedInWorkingDirectory("testfile-1mb.dat", "testfile-1mb-2.dat", "testfile-1mb-3.dat");

    logout();
  }

}
