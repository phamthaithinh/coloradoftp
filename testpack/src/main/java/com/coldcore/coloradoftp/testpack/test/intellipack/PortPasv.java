package com.coldcore.coloradoftp.testpack.test.intellipack;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * @see com.coldcore.coloradoftp.testpack.test.genericbundle.PortPasv
 */
public class PortPasv extends com.coldcore.coloradoftp.testpack.test.genericbundle.PortPasv {

  private static Logger log = Logger.getLogger(PortPasv.class);


  protected void setUp() throws Exception {
    super.setUp("intellipack-beans.xml");
  }

}