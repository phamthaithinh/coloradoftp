package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.misc5.StringReaper;
import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;

import java.util.*;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

/**
 * Test if server properly lists items.
 * Testing LIST, NLST commands
 */
public class Listing extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(Listing.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testListing() throws Exception {
    command("list", "530");
    command("nlst", "530");

    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-2", "257", "250");
    command("mkd testdir-2/testdir-3", "257", "250");
    command("mkd testdir-2/testdir-4", "257", "250");

    createRemoteTextFile(112, remoteUsersPath+"/anonymous/testfile.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-1/testfile-2.txt", false);
    createRemoteTextFile(16, remoteUsersPath+"/anonymous/testdir-1/testfile-3.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2/testfile-A.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-B.txt", false);
    createRemoteTextFile(32003, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-C.txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);


    pasv();
    command("nlst", "150");
    String reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile.txt\r\n") || !reply.contains("testdir-1/\r\n") || !reply.contains("testdir-2/\r\n")) fail("Listing NO: "+reply);

    pasv();
    command("list", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile.txt\r\n") || !reply.contains("testdir-1\r\n") || !reply.contains("testdir-2\r\n")) fail("Listing NO: "+reply);


    command("cwd testdir-1", "250");

    pasv();
    command("nlst", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile-1.txt\r\n") || !reply.contains("testfile-2.txt\r\n") || !reply.contains("testfile-3.txt\r\n")) fail("Listing NO: "+reply);
    if (reply.length() != "testfile-1.txt\r\n".length()*3) fail("Listing size NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureListItem(reply, "testfile-1.txt", false, 1024, curdate);
    ensureListItem(reply, "testfile-2.txt", false, 256, curdate);
    ensureListItem(reply, "testfile-3.txt", false, 16, curdate);
    ensureListLineCount(reply, 3);
    log.info("Listing OK");


    command("cwd ../testdir-2", "250");

    pasv();
    command("nlst", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile-A.txt\r\n") || !reply.contains("testdir-3/\r\n") || !reply.contains("testdir-4/\r\n")) fail("Listing NO: "+reply);
    if (reply.length() != "testfile-A.txt\r\n".length()+"testdir-3/\r\n".length()*2) fail("Listing size NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureListItem(reply, "testfile-A.txt", false, 1024, curdate);
    ensureListItem(reply, "testdir-3", true, 0, curdate);
    ensureListItem(reply, "testdir-4", true, 0, curdate);
    ensureListLineCount(reply, 3);
    log.info("Listing OK");


    command("cwd testdir-3", "250");

    pasv();
    command("nlst", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile-B.txt\r\n") || !reply.contains("testfile-C.txt\r\n")) fail("Listing NO: "+reply);
    if (reply.length() != "testfile-B.txt\r\n".length()*2) fail("Listing size NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureListItem(reply, "testfile-B.txt", false, 256, curdate);
    ensureListItem(reply, "testfile-C.txt", false, 32003, curdate);
    ensureListLineCount(reply, 2);
    log.info("Listing OK");


    command("cwd ../testdir-4", "250");

    pasv();
    command("nlst", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.equals("")) fail("Listing NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.equals("")) fail("Listing NO: "+reply);
    log.info("Listing OK");


    logout();
  }


  public void testListingSubDir() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-2", "257", "250");
    command("mkd testdir-2/testdir-3", "257", "250");
    command("mkd testdir-2/testdir-4", "257", "250");

    createRemoteTextFile(112, remoteUsersPath+"/anonymous/testfile.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-1/testfile-2.txt", false);
    createRemoteTextFile(16, remoteUsersPath+"/anonymous/testdir-1/testfile-3.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2/testfile-A.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-B.txt", false);
    createRemoteTextFile(32003, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-C.txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);
    String reply;

    command("nlst crazy", "450");
    command("list crazy", "450");

    pasv();
    command("nlst /", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile.txt\r\n") || !reply.contains("testdir-1/\r\n") || !reply.contains("testdir-2/\r\n")) fail("Listing NO: "+reply);

    pasv();
    command("list /", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile.txt\r\n") || !reply.contains("testdir-1\r\n") || !reply.contains("testdir-2\r\n")) fail("Listing NO: "+reply);


    pasv();
    command("nlst testdir-1", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile-1.txt\r\n") || !reply.contains("testfile-2.txt\r\n") || !reply.contains("testfile-3.txt\r\n")) fail("Listing NO: "+reply);
    if (reply.length() != "testfile-1.txt\r\n".length()*3) fail("Listing size NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list testdir-1", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureListItem(reply, "testfile-1.txt", false, 1024, curdate);
    ensureListItem(reply, "testfile-2.txt", false, 256, curdate);
    ensureListItem(reply, "testfile-3.txt", false, 16, curdate);
    ensureListLineCount(reply, 3);
    log.info("Listing OK");


    command("cwd testdir-1", "250");

    pasv();
    command("nlst ../testdir-2", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile-A.txt\r\n") || !reply.contains("testdir-3/\r\n") || !reply.contains("testdir-4/\r\n")) fail("Listing NO: "+reply);
    if (reply.length() != "testfile-A.txt\r\n".length()+"testdir-3/\r\n".length()*2) fail("Listing size NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list ../testdir-2", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureListItem(reply, "testfile-A.txt", false, 1024, curdate);
    ensureListItem(reply, "testdir-3", true, 0, curdate);
    ensureListItem(reply, "testdir-4", true, 0, curdate);
    ensureListLineCount(reply, 3);
    log.info("Listing OK");


    pasv();
    command("nlst /testdir-2/testdir-3", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile-B.txt\r\n") || !reply.contains("testfile-C.txt\r\n")) fail("Listing NO: "+reply);
    if (reply.length() != "testfile-B.txt\r\n".length()*2) fail("Listing size NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list /testdir-2/testdir-3", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureListItem(reply, "testfile-B.txt", false, 256, curdate);
    ensureListItem(reply, "testfile-C.txt", false, 32003, curdate);
    ensureListLineCount(reply, 2);
    log.info("Listing OK");


    command("cwd /", "250");

    pasv();
    command("nlst testdir-2/testdir-4", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.equals("")) fail("Listing NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list testdir-2/testdir-4", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.equals("")) fail("Listing NO: "+reply);
    log.info("Listing OK");


    command("nlst testdir-2/testdir-5", "450");
    command("list testdir-2/testdir-5", "450");


    logout();
  }


  public void testListingSpace() throws Exception {
    anonymousLogin();

    command("mkd testdir-2 [bolt]", "257", "250");
    command("mkd testdir-2 [bolt]/testdir-3 [bolt]", "257", "250");
    command("mkd testdir-2 [bolt]/testdir-4 [bolt]", "257", "250");

    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2 [bolt]/testfile-A [bolt].txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);
    String reply;


    command("cwd testdir-2 [bolt]", "250");

    pasv();
    command("nlst", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile-A [bolt].txt\r\n") || !reply.contains("testdir-3 [bolt]/\r\n") || !reply.contains("testdir-4 [bolt]/\r\n")) fail("Listing NO: "+reply);
    if (reply.length() != "testfile-A [bolt].txt\r\n".length()+"testdir-3 [bolt]/\r\n".length()*2) fail("Listing size NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureListItem(reply, "testfile-A [bolt].txt", false, 1024, curdate);
    ensureListItem(reply, "testdir-3 [bolt]", true, 0, curdate);
    ensureListItem(reply, "testdir-4 [bolt]", true, 0, curdate);
    ensureListLineCount(reply, 3);
    log.info("Listing OK");


    command("cwd testdir-4 [bolt]", "250");

    pasv();
    command("nlst", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.equals("")) fail("Listing NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.equals("")) fail("Listing NO: "+reply);
    log.info("Listing OK");


    logout();
  }


  public void testListingSubDirSpace() throws Exception {
    anonymousLogin();

    command("mkd testdir-2 [bolt]", "257", "250");
    command("mkd testdir-2 [bolt]/testdir-3 [bolt]", "257", "250");
    command("mkd testdir-2 [bolt]/testdir-4 [bolt]", "257", "250");

    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2 [bolt]/testfile-A [bolt].txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);
    String reply;


    pasv();
    command("nlst testdir-2 [bolt]", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.contains("testfile-A [bolt].txt\r\n") || !reply.contains("testdir-3 [bolt]/\r\n") || !reply.contains("testdir-4 [bolt]/\r\n")) fail("Listing NO: "+reply);
    if (reply.length() != "testfile-A [bolt].txt\r\n".length()+"testdir-3 [bolt]/\r\n".length()*2) fail("Listing size NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list testdir-2 [bolt]", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureListItem(reply, "testfile-A [bolt].txt", false, 1024, curdate);
    ensureListItem(reply, "testdir-3 [bolt]", true, 0, curdate);
    ensureListItem(reply, "testdir-4 [bolt]", true, 0, curdate);
    ensureListLineCount(reply, 3);
    log.info("Listing OK");


    pasv();
    command("nlst testdir-2 [bolt]/testdir-4 [bolt]", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.equals("")) fail("Listing NO: "+reply);
    log.info("Listing OK");

    pasv();
    command("list testdir-2 [bolt]/testdir-4 [bolt]", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    if (!reply.equals("")) fail("Listing NO: "+reply);
    log.info("Listing OK");


    logout();
  }


  private void ensureListItem(String reply, String item, boolean dir, long size, Date date) throws Exception {
    StringReaper sr = new StringReaper(reply);
    String[] val = sr.getValues(null, "\r\n");
    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd HH:mm", new Locale("en"));
    Calendar cal = new GregorianCalendar();
    cal.setTime(date);
    int year = cal.get(Calendar.YEAR);
    for (String s : val) {
      if (!s.endsWith(" "+item)) continue;
      sr.setContent(s);
      val = sr.getValues(null, " ");
      if (val.length < 8) fail("List item '"+item+"' failed: "+reply);
      String perm = val[0];
      String len = val[3];
      String dt = val[4]+" "+val[5]+" "+val[6];
      if (dir && perm.charAt(0) != 'd') fail("List item '"+item+"' type failed: "+reply);
      if (!dir && perm.charAt(0) != '-') fail("List item '"+item+"' type failed: "+reply);
      if (!dir && Long.parseLong(len) != size) fail("List item '"+item+"' size failed: "+reply);
      Date itemDate = sdf.parse(dt);
      cal.setTime(itemDate);
      cal.set(Calendar.YEAR, year);
      cal.add(Calendar.MINUTE, 2);
      if (cal.getTime().compareTo(date) < 0) fail("List item '"+item+"' date failed: "+reply);
      cal.add(Calendar.MINUTE, -3);
      if (cal.getTime().compareTo(date) > 0) fail("List item '"+item+"' date failed: "+reply);
      if (!item.startsWith(val[7])) fail("List item '"+item+"' name failed: "+reply);
      return;
    }
    fail("Item '"+item+"' not listed: "+reply);
  }


  private void ensureListLineCount(String reply, int count) {
    StringReaper sr = new StringReaper(reply);
    String[] val = sr.getValues(null, "\r\n");
    if (count != val.length) fail("Listing line count does not match ("+count+"/"+val.length+"): "+reply);
  }

}
