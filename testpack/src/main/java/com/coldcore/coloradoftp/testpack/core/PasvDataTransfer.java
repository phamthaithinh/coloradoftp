package com.coldcore.coloradoftp.testpack.core;

import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Reads/sends data from/to FTP server by using PASV mode.
 */
public class PasvDataTransfer {

  private static Logger log = Logger.getLogger(PasvDataTransfer.class);
  private Socket sc;
  private InputStream sin;
  private OutputStream sou;
  private String ip;
  private int port;
  private long time;
  private long datatime;
  private boolean busy;
  private boolean logged;


  /**
   * @param ip Server IP
   * @param port Server data port
   */
  public PasvDataTransfer(String ip, int port) {
    this.ip = ip;
    this.port = port;
    logged = true;
  }


  public void setLogged(boolean logged) {
    this.logged = logged;
  }


  /**
   * Read data from a server
   * @return Server's data
   */
  synchronized public byte[] readData() throws Exception {
    openConnection();
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    int i;
    byte[] b = new byte[4096];
    busy = true;
    long start = System.currentTimeMillis();
    long datastart = 0;
    while ((i = sin.read(b)) != -1) {
      if (datastart == 0) datastart = System.currentTimeMillis();
      bout.write(b, 0, i);
      Thread.yield();
    }
    datatime = System.currentTimeMillis()-datastart;
    time = System.currentTimeMillis()-start;
    busy = false;
    closeConnection();
    return bout.toByteArray();
  }


  /**
   * Read data from a server and dispose of it straight away (used for large files to save memory)
   * @return How many bytes were read
   */
  synchronized public long readDataHollow() throws Exception {
    openConnection();
    long size = 0;
    int i;
    byte[] b = new byte[4096];
    busy = true;
    long start = System.currentTimeMillis();
    long datastart = 0;
    while ((i = sin.read(b)) != -1) {
      if (datastart == 0) datastart = System.currentTimeMillis();
      size += i;
      Thread.yield();
    }
    datatime = System.currentTimeMillis()-datastart;
    time = System.currentTimeMillis()-start;
    busy = false;
    closeConnection();
    return size;
  }


  /**
   * Read data from a server
   * @return Server's data as a string
   */
  synchronized public String readDataAsString() throws Exception {
    return new String(readData(), "UTF-8");

  }


  /**
   * Send data to a server
   * @param data Data to send
   */
  synchronized public void writeData(byte[] data) throws Exception {
    openConnection();
    InputStream in = new ByteArrayInputStream(data);
    int i;
    byte[] b = new byte[4096];
    busy = true;
    long start = System.currentTimeMillis();
    long datastart = 0;
    while ((i = in.read(b)) != -1) {
      if (datastart == 0) datastart = System.currentTimeMillis();
      sou.write(b, 0, i);
      Thread.yield();
    }
    datatime = System.currentTimeMillis()-datastart;
    time = System.currentTimeMillis()-start;
    busy = false;
    closeConnection();
  }


  /**
   * Send dummy data to a server and dispose of it straight away (used for large files to save memory).
   * Note: Data to be sent contains repeated bytes [0..100]
   * @param size How many bytes to send
   */
  synchronized public void writeDataHollow(long size) throws Exception {
    openConnection();
    byte[] data = new byte[4096];
    byte b = 0;
    for (int z = 0; z < data.length; z++) {
      data[z] =  b;
      if (++b >= 100) b = 0;
    }
    busy = true;
    long start = System.currentTimeMillis();
    long datastart = 0;
    while (size > 0) {
      if (datastart == 0) datastart = System.currentTimeMillis();
      int len = data.length;
      if (len > size) len = (int) size;
      sou.write(data, 0, len);
      size -= len;
      Thread.yield();
    }
    datatime = System.currentTimeMillis()-datastart;
    time = System.currentTimeMillis()-start;
    busy = false;
    closeConnection();
  }


  /**
   * Send dummy data to a server and dispose of it straight away (used for large files to save memory).
   * Note: Data to be sent contains repeated line feeds \n or \r\n
   * @param size How many bytes to send
   * @param win TRUE for \r\n (size%2=0) and FALSE for \n
   */
  synchronized public void writeEmptyDataHollow(long size, boolean win) throws Exception {
    openConnection();
    byte[] data = new byte[4096];
    for (int z = 0; z < data.length; z++) {
      if (win) {
        data[z] = 13;
        z++;
      }
      data[z] = 10;
    }
    busy = true;
    long start = System.currentTimeMillis();
    long datastart = 0;
    while (size > 0) {
      if (datastart == 0) datastart = System.currentTimeMillis();
      int len = data.length;
      if (len > size) len = (int) size;
      sou.write(data, 0, len);
      size -= len;
      Thread.yield();
    }
    datatime = System.currentTimeMillis()-datastart;
    time = System.currentTimeMillis()-start;
    busy = false;
    closeConnection();
  }


  /**
   * Send a string to a server
   * @param data String to send
   */
  synchronized public void writeDataAsString(String data) throws Exception {
    writeData(data.getBytes("UTF-8"));
  }


  /** Connect to a server */
  synchronized public void openConnection() throws Exception {
    if (sc != null) return;
    sc = new Socket(ip, port);
    sin = sc.getInputStream();
    sou = sc.getOutputStream();
    if (logged) log.info("Data transfer connected");
    Thread.sleep(100);
  }


  /** Disconnect from a server */
  synchronized public void closeConnection() {
    try {
      sou.close();
    } catch (Throwable e) {}
    try {
      sin.close();
    } catch (Throwable e) {}
    try {
      sc.close();
    } catch (Throwable e) {}

    sc = null;
    if (logged) log.info("Data transfer disconnected");

    try {
      Thread.sleep(100);
    } catch (Throwable e) {}
  }


  /**
   * Get overall time spent during the last data transfer
   * @return Time in mills
   */
  public long getTime() {
    return time;
  }


  /**
   * Get precise time spent during the last data transfer (from the moment data actually started to flow)
   * @return Time in mills
   */
  public long getDatatime() {
    return datatime;
  }


  /** Test if this class is busy transferring data */
  public boolean isBusy() {
    return busy;
  }


  /** Test if connection is still open */
  public boolean isConnected() {
    return sc != null;
  }
}
