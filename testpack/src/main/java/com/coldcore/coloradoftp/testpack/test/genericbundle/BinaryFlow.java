package com.coldcore.coloradoftp.testpack.test.genericbundle;

import org.apache.log4j.Logger;
import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;

/**
 * Test binary transfer commands REST, REST, APPE, STOR with big 1MB files.
 */
public class BinaryFlow extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(BinaryFlow.class);


  protected void setUp() throws Exception {
    this.setUp("genericbundle-beans.xml");
  }


  protected void setUp(String beansFilename) throws Exception {
    this.beansFilename = beansFilename;
    super.setUp();
  }


  public void testReadWriteList() throws Exception {
    anonymousLogin();

    createRemoteBinaryFile(MBx1, remoteUsersPath+"/anonymous/testfile-1mb.dat");

    ensureListedInWorkingDirectory("testfile-1mb.dat");

    byte[] bdata;

    switchToBinaryMode();

    pasv();
    command("retr testfile-1mb.dat", "150");
    bdata = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1mb.dat", bdata);

    pasv();
    command("stor testfile-1mb-2.dat", "150");
    pasvTransfer.writeData(bdata);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1mb-2.dat", bdata);

    ensureListedInWorkingDirectory("testfile-1mb.dat", "testfile-1mb-2.dat");

    logout();
  }


  public void testReadResume() throws Exception {
    anonymousLogin();

    createRemoteBinaryFile(MBx1, remoteUsersPath+"/anonymous/testfile-1mb.dat");

    byte[] bdata;

    switchToBinaryMode();

    pasv();
    command("retr testfile-1mb.dat", "150");
    bdata = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1mb.dat", bdata);

    byte[] half = new byte[(int)MBx1/2];
    System.arraycopy(bdata, 0, half, 0, half.length);

    pasv();
    command("rest "+half.length, "350");
    command("retr testfile-1mb.dat", "150");
    bdata = pasvTransfer.readData();
    dataTransferComplete();

    byte[] complete = new byte[half.length+bdata.length];
    System.arraycopy(half, 0, complete, 0, half.length);
    System.arraycopy(bdata, 0, complete, half.length, bdata.length);
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1mb.dat", complete);

    logout();
  }


  public void testWriteResume() throws Exception {
    anonymousLogin();

    createRemoteBinaryFile(MBx1, remoteUsersPath+"/anonymous/testfile-1mb.dat");

    byte[] bdata;

    switchToBinaryMode();

    pasv();
    command("retr testfile-1mb.dat", "150");
    bdata = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1mb.dat", bdata);

    byte[] half1 = new byte[(int)MBx1/2];
    System.arraycopy(bdata, 0, half1, 0, half1.length);
    byte[] half2 = new byte[(int)MBx1/2];
    System.arraycopy(bdata, half1.length, half2, 0, half2.length);

    pasv();
    command("stor testfile-1mb-2.dat", "150");
    pasvTransfer.writeData(half1);
    dataTransferComplete();

    pasv();
    command("appe testfile-1mb-2.dat", "150");
    pasvTransfer.writeData(half2);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1mb-2.dat", bdata);

    logout();
  }

}
