package com.coldcore.coloradoftp.testpack.core;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.net.BindException;

import com.coldcore.misc5.IdGenerator;

/**
 * FTP client with the mind of its own. The client performs random operations.
 * All the exceptions will be aaded to the "errors" set instead of being thrown at runtime.
 * This class uses the Apache FTP client to perform operations.
 * Warning! If used in cuncurrent tests the Apache FTP client will most likely fail.
 */
public class RandomClient {

    private static Logger log = Logger.getLogger(RandomClient.class);
    private FTPClient ftpClient;
    private String host;
    private int port;
    private int maxErrors = 2;
    private String username;
    private String password;
    private Set<Throwable> errors;
    private Thread thr;
    private boolean running;
    private boolean connected;
    private boolean case1;
    private long started;
    private long stopped;

    protected String lorem = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, " +
                "sed diam nonumy eirmod tempor invidunt ut labore et dolore magna " +
                "aliquyam erat, sed diam voluptua. At vero eos et accusam et justo " +
                "duo dolores et ea rebum.";


    public RandomClient(String host, int port, String username, String password) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        errors = new LinkedHashSet<Throwable>();
        ftpClient = new FTPClient();
    }


    synchronized public void start() {
        if (running) return;
        running = true;

        errors.clear();
        case1 = false;
        started = System.currentTimeMillis();

        Runnable r = new Runnable() {
            public void run() {
                connect();
                while (running) {
                    doRandomAction();
                }
                disconnect();
            }
        };

        thr = new Thread(r);
        thr.start();
    }


    synchronized public void stop() {
        if (!running) return;
        running = false;

        stopped = System.currentTimeMillis();
    }


    private void doRandomAction() {
        try {

            if (errors.size() < maxErrors) {

                int r = (int)(Math.random()*(double)10)+1;

                switch (r) {
                    case 1: makeDirectory(); break;
                    case 2: deleteContent(); break;
                    case 3: renameContent(); break;
                    case 4: uploadFile(); break;
                    case 5: downloadFile(); break;
                    default: changeDirectory();
                }

            } else {
                stop();
            }

        } catch (Throwable e) {
            if (e instanceof BindException) case1 = true; //Client tries to bind to already-in-use local port (CASE1)
            else errors.add(e);
        }


        try {
            Thread.sleep(10);
        } catch (Throwable e) {}
    }


    public void join() {
        try {
            thr.join(30000);
        } catch (Throwable e) {}
    }


    private void connect() {
        if (connected) return;

        try {

            for (int z = 0; z < 5; z++)
                try {
                    ftpClient.connect(host, port);
                } catch (BindException e) {} //CASE1
            
            connected = true;

            if (!ftpClient.login(username, password)) throw new Exception("Login failed: "+ftpClient.getReplyString());

            ftpClient.setFileTransferMode(FTPClient.BINARY_FILE_TYPE);
            ftpClient.enterLocalPassiveMode();

        } catch (Throwable e) {
            errors.add(e);
        }
    }


    private void disconnect() {
        if (!connected) return;
        connected = false;

        try {
            if (!ftpClient.logout()) {
                //Because of CASE1 this 425 may happen
                if (ftpClient.getReplyCode() == 425 && case1);
                else throw new Exception("Logout failed: "+ftpClient.getReplyString());
            }
        } catch (Throwable e) {
            errors.add(e);
        }

        try {
            ftpClient.disconnect();
        } catch (Throwable e) {
            errors.add(e);
        }
    }


    public Set<Throwable> getErrors() {
        return errors;
    }


    public String getUsername() {
        return username;
    }


    public boolean isRunning() {
        return running;
    }


    public long getStarted() {
        return started;
    }


    public long getStopped() {
        return stopped;
    }


    /*** Actions ***/

    private void changeDirectory() throws Exception {
        List<String> dirs = new ArrayList<String>();
        dirs.add("..");

        FTPFile[] list = ftpClient.listFiles();
        for (FTPFile f : list)
            if (f.isDirectory()) dirs.add(f.getName());

        int r = (int)(Math.random()*(double)dirs.size());
        if (r >= dirs.size()) r = dirs.size()-1;

        if (r == 0) {
            if (!ftpClient.changeToParentDirectory())
                errors.add(new Exception("Failed to change to parent directory: "+ftpClient.getReplyString()));
        } else {
            if (!ftpClient.changeWorkingDirectory(dirs.get(r)))
                errors.add(new Exception("Failed to change to directory: "+ftpClient.getReplyString()));
        }

        //log.info("Client '"+username+"' changed directory: "+ftpClient.printWorkingDirectory());
    }


    private void deleteContent() throws Exception {
        FTPFile[] list = ftpClient.listFiles();
        if (list.length == 0) return;

        int r = (int)(Math.random()*(double)list.length);
        if (r >= list.length) r = list.length-1;

        if (!ftpClient.deleteFile(list[r].getName()))
            errors.add(new Exception("Failed to delete item: "+ftpClient.getReplyString()));

        //log.info("Client '"+username+"' deleted: "+list[r].getName());
    }


    private void renameContent() throws Exception {
        FTPFile[] list = ftpClient.listFiles();
        if (list.length == 0) return;

        int r = (int)(Math.random()*(double)list.length);
        if (r >= list.length) r = list.length-1;

        String name = ""+System.currentTimeMillis()+IdGenerator.generate(4,4);

        if (!ftpClient.rename(list[r].getName(), name))
            errors.add(new Exception("Failed to rename item: "+ftpClient.getReplyString()));

        //log.info("Client '"+username+"' renamed: "+list[r].getName()+" -> "+name);
    }


    private void makeDirectory() throws Exception {
        String dir = ""+System.currentTimeMillis()+IdGenerator.generate(4,4);
        if (!ftpClient.makeDirectory(dir))
            errors.add(new Exception("Failed to make directory: "+ftpClient.getReplyString()));

        //log.info("Client '"+username+"' made directory: "+dir);
    }


    private void uploadFile() throws Exception {
        String filename = System.currentTimeMillis()+IdGenerator.generate(4,4)+".dat";
        if (!ftpClient.storeFile(filename, new ByteArrayInputStream(lorem.getBytes())))
            errors.add(new Exception("Failed to upload file: "+ftpClient.getReplyString()));

        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        if (!ftpClient.retrieveFile(filename, bout))
            errors.add(new Exception("Failed to download uploaded file: "+ftpClient.getReplyString()));

        String content = new String(bout.toByteArray());
        if (!content.equals(lorem))
            errors.add(new Exception("Uploaded file did not match downloaded file"));

        //log.info("Client '"+username+"' uploaded file: "+filename);
    }


    private void downloadFile() throws Exception {
        List<String> files = new ArrayList<String>();

        FTPFile[] list = ftpClient.listFiles();
        for (FTPFile f : list)
            if (f.isFile()) files.add(f.getName());

        if (files.size() == 0) return;

        int r = (int)(Math.random()*(double)files.size());
        if (r >= files.size()) r = files.size()-1;

        if (!ftpClient.retrieveFile(files.get(r), new ByteArrayOutputStream()))
            errors.add(new Exception("Failed to download file: "+ftpClient.getReplyString()));

        //log.info("Client '"+username+"' downloaded file: "+files.get(r));
    }

}
