package com.coldcore.coloradoftp.testpack.core;

import com.coldcore.misc5.CByte;
import com.coldcore.misc5.StringReaper;
import org.apache.commons.net.ftp.FTPFile;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.net.SocketException;

/**
 * Custom FTP client.
 *
 * This client was made for cuncurrent tests because the Apache FTP client fails
 * too often during cuncurrent tests. The class throws exception on every problem 
 * it encounters.
 */
public class CustomClient {

    private Communicator communicator;
    private PasvDataTransfer pasvTransfer;
    private String host;
    private int port;


    public CustomClient(String host, int port) {
        this.host = host;
        this.port = port;
        communicator = new Communicator(host, port);
        communicator.setLogged(false);
    }


    public void connect() throws Exception {
        communicator.openConnection();
        String reply = communicator.nextReply();
        if (!reply.startsWith("220")) throw new Exception("Remote server did not accept connection: "+reply);
    }


    public void disconnect() {
        communicator.closeConnection();
    }


    private String command(String input, String... responseCode) throws Exception {
      communicator.send(input);
      String reply = communicator.nextReply();
      for (String code : responseCode) {
        if (reply.equals(code) || reply.startsWith(code+" ") || reply.startsWith(code+"- "))
          return reply;
      }
      throw new Exception("Invalid reply: "+input+" -> "+reply);
    }


    private void pasv() throws Exception {
      String reply = command("pasv", "227");

      if (pasvTransfer != null && pasvTransfer.isConnected())
        throw new Exception("PASV data transfer already in progress");

      StringTokenizer st = new StringTokenizer(reply, "()");
      st.nextToken();
      String addr = st.nextToken();
      st = new StringTokenizer(addr, ",");
      String ip = st.nextToken()+"."+st.nextToken()+"."+st.nextToken()+"."+st.nextToken();
      int port = Integer.parseInt(st.nextToken())*256+Integer.parseInt(st.nextToken());

      pasvTransfer = new PasvDataTransfer(ip, port);
      pasvTransfer.setLogged(false);
    }


    private void dataTransferComplete() throws Exception {
      String reply = communicator.nextReply();
      if (!reply.startsWith("226 ") && !reply.startsWith("250 ")) throw new Exception("Data transfer failed: "+reply);
      if (pasvTransfer != null && pasvTransfer.isBusy()) throw new Exception("PASV data transfer object is busy after transfer");
    }


    private Set<FTPFile> parseList(String reply) throws Exception {
      Set<FTPFile> set = new HashSet<FTPFile>();
      if (reply.trim().length() == 0) return set; //Empty directory listed
      StringReaper sr = new StringReaper(reply);
      String[] val = sr.getValues(null, "\r\n");
      SimpleDateFormat sdf = new SimpleDateFormat("MMM dd HH:mm", new Locale("en"));
      for (String s : val) {
        FTPFile ftpFile = new FTPFile();
        sr.setContent(s);
        val = sr.getValues(null, " ");
        if (val.length < 8) throw new Exception("List failed: "+reply);
        String perm = val[0];
        String len = val[3];
        String dt = val[4]+" "+val[5]+" "+val[6];
        if (perm.charAt(0) == 'd') ftpFile.setType(FTPFile.DIRECTORY_TYPE);
        else if (perm.charAt(0) == '-') ftpFile.setType(FTPFile.FILE_TYPE);
        else throw new Exception("List type failed: "+reply);
        ftpFile.setSize(Long.parseLong(len));
        Date itemDate = sdf.parse(dt);
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(itemDate);
        ftpFile.setTimestamp(cal);
        cal.setTime(itemDate);
        ftpFile.setName(val[7]); //Ignore the last "/" in directories
        set.add(ftpFile);
      }
      return set;
    }


    private void switchToBinaryMode() throws Exception {
      command("type I", "200");
    }


    public void changeWorkingDirectory(String dir) throws Exception {
        command("cwd "+dir, "250");
    }


    public String printWorkingDirectory() throws Exception {
        String reply = command("pwd", "257");
        int i1 = reply.indexOf("\"");
        int i2 = reply.lastIndexOf("\"");
        return reply.substring(i1+1, i2);
    }


    public void changeToParentDirectory() throws Exception {
        command("cdup", "250");
    }


    public void login(String username, String password) throws Exception {
        command("user "+username, "331");
        command("pass "+password, "230");
    }


    public void logout() throws Exception {
        command("quit", "221");
        communicator.nextReply();
        if (communicator.isOpen()) throw new Exception("Connection left open after logout");
    }


    public void delete(String path) throws Exception {
        command("dele "+path, "250");
    }


    public void rename(String from, String to) throws Exception {
        command("rnfr "+from, "350");
        command("rnto "+to, "250");
    }


    public void makeDirectory(String dir) throws Exception {
        command("mkd "+dir, "257", "250");
    }


    public void storeFile(String filename, InputStream in) throws Exception {
        switchToBinaryMode();
        pasv();
        command("stor "+filename, "150");
        byte[] data = CByte.toByteArray(in);
        pasvTransfer.writeData(data);
        dataTransferComplete();
    }


    public void retrieveFile(String filename, OutputStream out) throws Exception {
        switchToBinaryMode();
        pasv();
        command("retr "+filename, "150");
        byte[] data = pasvTransfer.readData();
        dataTransferComplete();
        CByte.inputToOutput(new ByteArrayInputStream(data), out);
    }


    public FTPFile[] listFiles() throws Exception {
        pasv();
        command("list", "150");
        String reply = pasvTransfer.readDataAsString();
        dataTransferComplete();
        return parseList(reply).toArray(new FTPFile[0]);
    }

}
