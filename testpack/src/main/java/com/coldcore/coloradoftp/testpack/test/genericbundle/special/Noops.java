package com.coldcore.coloradoftp.testpack.test.genericbundle.special;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Testing server echos to make sure the server does not swallow any symbols.
 * Tested special NOOP implementation (regular NOOP does not echo)
 */
public class Noops extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(Noops.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testNoop() throws Exception {
    String reply;

    communicator.send("noop");
    reply = communicator.nextReply();
    if (!reply.equals("200 OK\r\n")) fail("Noop NO: "+reply);
    log.info("Noop OK");

    communicator.send("noop 123");
    reply = communicator.nextReply();
    if (!reply.equals("200 OK 123\r\n")) fail("Noop NO: "+reply);
    log.info("Noop OK");
  }


  public void testNoops() throws Exception {
    doNOOP(100);
    doNOOP(1000);
    doNOOP(4000);
    doNOOP(8000);
    doNOOP(10000);
    doNOOP(15000);
  }


  public void testNoopsUTF8() throws Exception {
    doNOOPUTF8(100);
    doNOOPUTF8(1000);
    doNOOPUTF8(4000);
    doNOOPUTF8(8000);
    doNOOPUTF8(10000);
    doNOOPUTF8(15000);
  }


  /**
   * Test NOOP
   * @param len Echo length
   */
  private void doNOOP(int len) throws Exception {
    String reply;

    String param = "";
    int num = 0;
    for (int j = 0; j < len; j++) {
      param += ""+num;
      if (++num > 9) num = 0;
    }

    communicator.send("noop "+param);
    reply = communicator.nextReply();
    if (!reply.equals("200 OK "+param+"\r\n")) fail("Noop NO: "+reply);
    log.info("Noop OK");
  }


  /**
   * Test NOOP with UTF-8 echo
   * @param len Echo length
   */
  private void doNOOPUTF8(int len) throws Exception {
    String reply;

    char[] carr = new char[]{1055,1088,1077,1074,1077,1076,'_',1052,1077,1076,1074,1077,1076};

    String param = "";
    int ind = 0;
    for (int j = 0; j < len; j++) {
      param += ""+carr[ind];
      if (++ind >= carr.length) ind = 0;
    }

    communicator.send("noop "+param);
    reply = communicator.nextReply();
    if (!reply.equals("200 OK "+param+"\r\n")) fail("Noop NO: "+reply);
    log.info("Noop OK");
  }

}
