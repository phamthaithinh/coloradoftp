package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test behaviour of STOR/STOU/APPE commands.
 */
public class FileUpload extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(FileUpload.class);


  protected void setUp() throws Exception {
    this.setUp("genericbundle-beans.xml");
  }


  protected void setUp(String beansFilename) throws Exception {
    this.beansFilename = beansFilename;
    super.setUp();
  }


  public void testPasvStor() throws Exception {
    command("stor", "530");

    anonymousLogin();

    command("stor", "501");
    command("stor testfile-1.txt", "425");

    pasv();
    command("stor testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("\n\n\n\nHello there\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", "\n\n\n\nHello there\n\n".getBytes());

    command("stor testfile.txt", "425");

    pasv();
    command("stor testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("Hello there,\r\n\r\n\r\nBuy now!\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", "Hello there,\n\n\nBuy now!\n\n".getBytes());

    switchToAsciiMode();
    pasv();
    command("stor testfile-2.txt", "150");
    pasvTransfer.writeDataAsString("Hello\r\nthere,\r\nBuy now, come again!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-2.txt", "Hello\nthere,\nBuy now, come again!\n".getBytes());

    switchToBinaryMode();
    pasv();
    command("stor testfile-3.txt", "150");
    pasvTransfer.writeDataAsString("\r\n\r\nHello there,\r\n\r\nBuy now!\n\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-3.txt", "\r\n\r\nHello there,\r\n\r\nBuy now!\n\n".getBytes());

    logout();
  }


  public void testPasvAppe() throws Exception {
    command("appe", "530");

    anonymousLogin();

    switchToBinaryMode();
    command("appe", "501");
    command("appe testfile-1.txt", "425");

    pasv();
    command("appe testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("\n\n\n\nHello there\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", "\n\n\n\nHello there\r\n\r\n".getBytes());

    command("appe testfile.txt", "425");

    pasv();
    command("appe testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("Hello there,\r\n\r\n\r\nBuy now!\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", ("\n\n\n\nHello there\r\n\r\n"+"Hello there,\r\n\r\n\r\nBuy now!\n\r\n").getBytes());

    switchToAsciiMode();
    command("appe testfile-2.txt", "550");

    logout();
  }


  public void testPasvStou() throws Exception {
    command("stou", "530");

    anonymousLogin();

    command("stou", "501");

    command("mkd /testdir-A", "257", "250");
    command("cwd /testdir-A", "250");
    command("stou", "425");

    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("\n\n\n\nHello there\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", "\n\n\n\nHello there\n\n".getBytes());

    command("stou", "425");

    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("Hello there,\r\n\r\n\r\nBuy now!\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", "Hello there,\n\n\nBuy now!\n\n".getBytes());

    switchToAsciiMode();
    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("Hello\r\nthere,\r\nBuy now, come again!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", "Hello\nthere,\nBuy now, come again!\n".getBytes());

    switchToBinaryMode();
    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("\r\n\r\nHello there,\r\n\r\nBuy now!\n\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", "\r\n\r\nHello there,\r\n\r\nBuy now!\n\n".getBytes());

    logout();
  }


  public void testPasvStorSubdir() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");
    command("mkd /testdir-A/testdir-B", "257", "250");
    command("mkd /testdir-C", "257", "250");
    command("mkd /testdir-D", "257", "250");

    switchToBinaryMode();

    command("cwd /", "250");
    pasv();
    command("stor testdir-A/testdir-B/testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890");
    dataTransferComplete();

    command("cwd /testdir-C", "250");
    pasv();
    command("stor testfile-2.txt", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890");
    dataTransferComplete();

    command("cwd /", "250");
    pasv();
    command("stor /testdir-D/testfile-3.txt", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890");
    dataTransferComplete();

    command("cwd /", "250");
    pasv();
    command("stor /testdir-E/testfile-4.txt", "450");

    command("cwd /", "250");
    ensureNotListedInWorkingDirectory("testfile-1.txt");
    ensureNotListedInWorkingDirectory("testfile-2.txt");
    ensureNotListedInWorkingDirectory("testfile-3.txt");

    command("cwd /testdir-A/testdir-B", "250");
    ensureListedInWorkingDirectory("testfile-1.txt");

    command("cwd /testdir-C", "250");
    ensureListedInWorkingDirectory("testfile-2.txt");

    command("cwd /testdir-D", "250");
    ensureListedInWorkingDirectory("testfile-3.txt");

    logout();
  }


  public void testPasvAppeSubdir() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");
    command("mkd /testdir-A/testdir-B", "257", "250");
    command("mkd /testdir-C", "257", "250");
    command("mkd /testdir-D", "257", "250");

    switchToBinaryMode();

    command("cwd /", "250");
    pasv();
    command("appe testdir-A/testdir-B/testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890");
    dataTransferComplete();
    pasv();
    command("appe testdir-A/testdir-B/testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890"+"123456789012345678901234567890");
    dataTransferComplete();

    command("cwd /testdir-C", "250");
    pasv();
    command("appe testfile-2.txt", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890");
    dataTransferComplete();
    pasv();
    command("appe testfile-2.txt", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890"+"123456789012345678901234567890");
    dataTransferComplete();
    pasv();
    command("appe testfile-2.txt", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890"+"123456789012345678901234567890"+"123456789012345678901234567890");
    dataTransferComplete();

    command("cwd /", "250");
    pasv();
    command("appe /testdir-D/testfile-3.txt", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890");
    dataTransferComplete();

    command("cwd /", "250");
    pasv();
    command("appe /testdir-E/testfile-4.txt", "450");

    command("cwd /", "250");
    ensureNotListedInWorkingDirectory("testfile-1.txt");
    ensureNotListedInWorkingDirectory("testfile-2.txt");
    ensureNotListedInWorkingDirectory("testfile-3.txt");

    command("cwd /testdir-A/testdir-B", "250");
    ensureListedInWorkingDirectory("testfile-1.txt");

    command("cwd /testdir-C", "250");
    ensureListedInWorkingDirectory("testfile-2.txt");

    command("cwd /testdir-D", "250");
    ensureListedInWorkingDirectory("testfile-3.txt");

    logout();
  }


  public void testPasvStouSubdir() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");
    command("mkd /testdir-A/testdir-B", "257", "250");
    command("mkd /testdir-C", "257", "250");

    switchToBinaryMode();

    command("cwd /testdir-A/testdir-B", "250");
    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890");
    dataTransferComplete();

    command("cwd /testdir-C", "250");
    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("123456789012345678901234567890");
    dataTransferComplete();

    command("cwd /", "250");
    pasv();
    command("stou", "501");

    command("cwd /testdir-A/testdir-B", "250");
    ensureListedInWorkingDirectory("testdir-B");

    command("cwd /testdir-C", "250");
    ensureListedInWorkingDirectory("testdir-C");

    logout();
  }


  public void testPasvStorUTF8() throws Exception {
    anonymousLogin();

    char[] carr = new char[]{1055,1088,1077,1074,1077,1076,'_',1052,1077,1076,1074,1077,1076};
    String istr = "";
    for (char c : carr)
      istr += c;

    pasv();
    command("stor testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("\n\n\n\n"+istr+"\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", ("\n\n\n\n"+istr+"\n\n").getBytes("UTF-8"));

    command("stor testfile.txt", "425");

    pasv();
    command("stor testfile-1.txt", "150");
    pasvTransfer.writeDataAsString(istr+",\r\n\r\n\r\n"+istr+"!\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", (istr+",\n\n\n"+istr+"!\n\n").getBytes("UTF-8"));

    switchToAsciiMode();
    pasv();
    command("stor testfile-2.txt", "150");
    pasvTransfer.writeDataAsString(istr+"\r\n"+istr+",\r\n"+istr+", "+istr+"!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-2.txt", (istr+"\n"+istr+",\n"+istr+", "+istr+"!\n").getBytes("UTF-8"));

    switchToBinaryMode();
    pasv();
    command("stor testfile-3.txt", "150");
    pasvTransfer.writeDataAsString("\r\n\r\n"+istr+",\r\n\r\n"+istr+"!\n\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-3.txt", ("\r\n\r\n"+istr+",\r\n\r\n"+istr+"!\n\n").getBytes("UTF-8"));

    logout();
  }


  public void testPasvAppeUTF8() throws Exception {
    anonymousLogin();

    char[] carr = new char[]{1055,1088,1077,1074,1077,1076,'_',1052,1077,1076,1074,1077,1076};
    String istr = "";
    for (char c : carr)
      istr += c;

    switchToBinaryMode();

    pasv();
    command("appe testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("\n\n\n\n"+istr+"\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", ("\n\n\n\n"+istr+"\r\n\r\n").getBytes("UTF-8"));

    command("appe testfile.txt", "425");

    pasv();
    command("appe testfile-1.txt", "150");
    pasvTransfer.writeDataAsString(istr+",\r\n\r\n\r\n"+istr+"!\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", ("\n\n\n\n"+istr+"\r\n\r\n"+istr+",\r\n\r\n\r\n"+istr+"!\n\r\n").getBytes("UTF-8"));

    switchToAsciiMode();
    command("appe testfile-2.txt", "550");

    logout();
  }


  public void testPasvStouUTF8() throws Exception {
    anonymousLogin();

    char[] carr = new char[]{1055,1088,1077,1074,1077,1076,'_',1052,1077,1076,1074,1077,1076};
    String istr = "";
    for (char c : carr)
      istr += c;

    command("mkd /testdir-A", "257", "250");
    command("cwd /testdir-A", "250");

    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("\n\n\n\n"+istr+"\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", ("\n\n\n\n"+istr+"\n\n").getBytes("UTF-8"));

    command("stor testfile.txt", "425");

    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString(istr+",\r\n\r\n\r\n"+istr+"!\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", (istr+",\n\n\n"+istr+"!\n\n").getBytes("UTF-8"));

    switchToAsciiMode();
    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString(istr+"\r\n"+istr+",\r\n"+istr+", "+istr+"!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", (istr+"\n"+istr+",\n"+istr+", "+istr+"!\n").getBytes("UTF-8"));

    switchToBinaryMode();
    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("\r\n\r\n"+istr+",\r\n\r\n"+istr+"!\n\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", ("\r\n\r\n"+istr+",\r\n\r\n"+istr+"!\n\n").getBytes("UTF-8"));

    logout();
  }


  public void testSaveSubdir() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");

    pasv();
    command("stor testdir-1/testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("\n\n\n\nHello there\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", "\n\n\n\nHello there\n\n".getBytes());

    pasv();
    command("stor testdir-1/testfile-2 [bolt].txt", "150");
    pasvTransfer.writeDataAsString("\n\n\n\nHello there\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-1/testfile-2 [bolt].txt", "\n\n\n\nHello there\n\n".getBytes());

    switchToBinaryMode();

    pasv();
    command("appe testdir-1/testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("\n\n\n\nHello there\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", "\n\n\n\nHello there\n\n\n\n\n\nHello there\r\n\r\n".getBytes());

    pasv();
    command("appe testdir-1/testfile-2 [bolt].txt", "150");
    pasvTransfer.writeDataAsString("\n\n\n\nHello there\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-1/testfile-2 [bolt].txt", "\n\n\n\nHello there\n\n\n\n\n\nHello there\r\n\r\n".getBytes());

    logout();
  }
}
