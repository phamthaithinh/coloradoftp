package com.coldcore.coloradoftp.testpack.test.genericbundle.special;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test if the server closes data channels when client dies, aborts, quits, repeats save commands etc.
 * If the server fails to close file channels then file descriptors remain open and those files cannot
 * be deleted nor moved.
 */
public class SaneDirBrokenSave extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(SaneDirBrokenSave.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testClientQuitAfterStou() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");

    command("cwd /testdir-A", "250");
    command("stou", "425");

    logout();
  }


  public void testPasvClientQuitAfterStou() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");

    command("cwd /testdir-A", "250");
    pasv();
    command("stou", "150");

    communicator.send("quit");
    nextReplyCode("425");
    nextReplyCode("221");
  }


  public void testPasvClientQuitAfterStouWithDataCon() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");

    command("cwd /testdir-A", "250");
    pasv();
    pasvTransfer.openConnection();
    command("stou", "150");

    communicator.send("quit");
    nextReplyCode("221");

    pasvTransfer.closeConnection();
  }


  public void testClientQuitAfterStouX3() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");
    command("mkd /testdir-B", "257", "250");
    command("mkd /testdir-C", "257", "250");

    command("cwd /testdir-A", "250");
    command("stou", "425");

    command("cwd /testdir-B", "250");
    command("stou", "425");

    command("cwd /testdir-C", "250");
    command("stou", "425");

    logout();
  }


  public void testPasvClientAbortQuitAfterStouX3() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");
    command("mkd /testdir-B", "257", "250");
    command("mkd /testdir-C", "257", "250");

    command("cwd /testdir-A", "250");
    pasv();
    command("stou", "150");

    communicator.send("abor");
    nextReplyCode("425");
    nextReplyCode("226");

    command("cwd /testdir-B", "250");
    pasv();
    command("stou", "150");

    communicator.send("abor");
    nextReplyCode("425");
    nextReplyCode("226");

    command("cwd /testdir-C", "250");
    pasv();
    command("stou", "150");

    communicator.send("quit");
    nextReplyCode("425");
    nextReplyCode("221");
  }


  public void testPasvClientAborQuitAfterStouX3WithDataCon() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");
    command("mkd /testdir-B", "257", "250");
    command("mkd /testdir-C", "257", "250");

    command("cwd /testdir-A", "250");
    pasv();
    pasvTransfer.openConnection();
    command("stou", "150");

    communicator.send("abor");
    nextReplyCode("426");
    nextReplyCode("226");
    pasvTransfer.closeConnection();

    command("cwd /testdir-B", "250");
    pasv();
    pasvTransfer.openConnection();
    command("stou", "150");

    communicator.send("abor");
    nextReplyCode("426");
    nextReplyCode("226");
    pasvTransfer.closeConnection();

    command("cwd /testdir-C", "250");
    pasv();
    pasvTransfer.openConnection();
    command("stou", "150");

    communicator.send("quit");
    nextReplyCode("221");

    pasvTransfer.closeConnection();
  }


  public void testPasvClientDiesAfterStou() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");

    command("cwd /testdir-A", "250");
    pasv();
    command("stou", "150");
  }


  public void testPasvClientDiesAfterStouWithDataCon() throws Exception {
    anonymousLogin();

    command("mkd /testdir-A", "257", "250");

    command("cwd /testdir-A", "250");
    pasv();
    pasvTransfer.openConnection();
    command("stou", "150");
  }

}
