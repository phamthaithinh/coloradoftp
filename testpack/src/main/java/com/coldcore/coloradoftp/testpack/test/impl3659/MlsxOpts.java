package com.coldcore.coloradoftp.testpack.test.impl3659;

import org.apache.log4j.Logger;

import java.util.Date;
import java.util.Set;
import java.util.HashSet;

/**
 * Test if server properly modifies listing based on OPTS.
 * Testing FEAT/OPTS with MLST, MLSD commands.
 * This test assumes that directories in server response are in absolute form with
 * additional current and parent directories.
 */
public class MlsxOpts extends MlsxBase {

  private static Logger log = Logger.getLogger(MlsxOpts.class);


  protected void setUp() throws Exception {
    beansFilename = "impl3659-beans.xml";
    super.setUp();
  }


  public void testOptsFeatMlst() throws Exception {
    anonymousLogin();

    String reply;

    reply = command("feat mlst", "211");
    if (!reply.contains("type*;") || !reply.contains("size*;") ||
        !reply.contains("modify*;") || !reply.contains("perm*;")) fail("MLST features NO: "+reply);
    log.info("MLST features OK");

    command("opts mlst crazy", "501");
    command("opts mlst crazy;crazy size;", "501");

    reply = command("opts mlst crazy;size;modify;type;perm;", "200");
    if (!reply.contains("type;") || !reply.contains("size;") ||
        !reply.contains("modify;") || !reply.contains("perm;")) fail("MLST options NO: "+reply);
    log.info("MLST options OK");

    reply = command("feat mlst", "211");
    if (!reply.contains("type*;") || !reply.contains("size*;") ||
        !reply.contains("modify*;") || !reply.contains("perm*;")) fail("MLST features NO: "+reply);
    log.info("MLST features OK");

    reply = command("opts mlst size;modify;", "200");
    if (reply.contains("type;") || !reply.contains("size;") ||
        !reply.contains("modify;") || reply.contains("perm;")) fail("MLST options NO: "+reply);
    log.info("MLST options OK");

    reply = command("feat mlst", "211");
    if (!reply.contains("type;") || !reply.contains("size*;") ||
        !reply.contains("modify*;") || !reply.contains("perm;")) fail("MLST features NO: "+reply);
    log.info("MLST features OK");

    reply = command("opts mlst", "200");
    if (reply.contains("type;") || reply.contains("size;") ||
        reply.contains("modify;") || reply.contains("perm;")) fail("MLST options NO: "+reply);
    log.info("MLST options OK");

    reply = command("feat mlst", "211");
    if (!reply.contains("type;") || !reply.contains("size;") ||
        !reply.contains("modify;") || !reply.contains("perm;")) fail("MLST features NO: "+reply);
    log.info("MLST features OK");


    logout();
  }


  public void testOptsListing() throws Exception {
    anonymousLogin();

    command("mkd testdir-2", "257", "250");
    command("mkd testdir-2/testdir-3", "257", "250");

    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2/testfile-A.txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);
    String reply;

    command("opts mlst modify;type;perm;size;", "200");

    pasv();
    command("mlsd testdir-2", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-3", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 4);
    log.info("Listing OK");

    reply = command("mlst testdir-2", "250");
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");

    reply = command("mlst testdir-2/testfile-A.txt", "250");
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    log.info("Listing OK");


    logout();
  }


  public void testOptsListing2() throws Exception {
    anonymousLogin();

    command("mkd testdir-2", "257", "250");
    command("mkd testdir-2/testdir-3", "257", "250");

    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2/testfile-A.txt", false);

    Date curdate = new Date();
    log.info("Using date "+curdate);
    String reply;

    Set<String> facts;

    facts = new HashSet<String>(this.facts);
    facts.remove("size");
    facts.remove("perm");
    command("opts mlst modify;type;", "200");

    pasv();
    command("mlsd testdir-2", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-3", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 4);
    if (reply.contains("size") || reply.contains("perm")) fail("Options NO: "+reply);
    log.info("Listing OK");

    reply = command("mlst testdir-2", "250");
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    if (reply.contains("size") || reply.contains("perm")) fail("Options NO: "+reply);
    log.info("Listing OK");

    reply = command("mlst testdir-2/testfile-A.txt", "250");
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    if (reply.contains("size") || reply.contains("perm")) fail("Options NO: "+reply);
    log.info("Listing OK");


    facts = new HashSet<String>(this.facts);
    facts.remove("modify");
    command("opts mlst size;type;perm;", "200");

    pasv();
    command("mlsd testdir-2", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-3", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 4);
    if (reply.contains("modify")) fail("Options NO: "+reply);
    log.info("Listing OK");

    reply = command("mlst testdir-2", "250");
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    if (reply.contains("modify")) fail("Options NO: "+reply);
    log.info("Listing OK");

    reply = command("mlst testdir-2/testfile-A.txt", "250");
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    if (reply.contains("modify")) fail("Options NO: "+reply);
    log.info("Listing OK");


    facts = new HashSet<String>();
    command("opts mlst", "200");

    pasv();
    command("mlsd testdir-2", "150");
    reply = pasvTransfer.readDataAsString();
    dataTransferComplete();
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2/testdir-3", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/", true, 0, curdate, facts);
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 4);
    if (reply.contains("size") || reply.contains("perm") || reply.contains("type") || reply.contains("modify")) fail("Options NO: "+reply);
    log.info("Listing OK");

    reply = command("mlst testdir-2", "250");
    ensureItemIsListed(reply, "/testdir-2", true, 0, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    if (reply.contains("size") || reply.contains("perm") || reply.contains("type") || reply.contains("modify")) fail("Options NO: "+reply);
    log.info("Listing OK");

    reply = command("mlst testdir-2/testfile-A.txt", "250");
    ensureItemIsListed(reply, "/testdir-2/testfile-A.txt", false, 1024, curdate, facts);
    ensureListLineCountMatches(reply, 3);
    if (reply.contains("size") || reply.contains("perm") || reply.contains("type") || reply.contains("modify")) fail("Options NO: "+reply);
    log.info("Listing OK");


    logout();
  }

}
