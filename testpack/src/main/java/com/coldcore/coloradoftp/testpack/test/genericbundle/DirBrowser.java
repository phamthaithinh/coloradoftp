package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test if server properly creates/deletes/browses directories and subdirectories.
 * Testing CWD, CDUP, MKD, RMD, PWD commands
 */
public class DirBrowser extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(DirBrowser.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testBasics() throws Exception {
    command("cwd", "530");
    command("cdup", "530");
    command("mkd", "530");
    command("rmd", "530");
    command("pwd", "530");

    anonymousLogin();

    command("mkd", "501");
    command("rmd", "501");

    command("cdup", "250");
    command("cwd", "250");
    command("cwd /", "250");
    command("pwd", "257");

    ensureWorkingDirectoryMatches("/");
    command("mkd testdir-1", "257", "250");
    command("cwd testdir-1", "250");
    ensureWorkingDirectoryMatches("/testdir-1");
    command("cdup", "250");
    ensureWorkingDirectoryMatches("/");
    command("rmd testdir-1", "250");

    command("cwd testdir-1", "450");
    command("rmd testdir-1", "450");

    command("cwd /", "250");
    ensureWorkingDirectoryMatches("/");
    command("mkd testdir-1 [bolt]", "257", "250");
    command("cwd testdir-1 [bolt]", "250");
    ensureWorkingDirectoryMatches("/testdir-1 [bolt]");
    command("cdup", "250");
    ensureWorkingDirectoryMatches("/");
    command("rmd testdir-1 [bolt]", "250");
    command("cwd testdir-1 [bolt]", "450");
    ensureWorkingDirectoryMatches("/");

    logout();
  }


  public void testBrowse() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-1/testdir-2", "257", "250");
    command("mkd testdir-1/testdir-2/testdir-3", "257", "250");

    command("cwd testdir-1", "250");
    command("cwd testdir-2", "250");
    command("cwd testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd testdir-A", "450");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cdup", "250");
    command("cdup", "250");
    ensureWorkingDirectoryMatches("/testdir-1");
    command("cdup", "250");
    ensureWorkingDirectoryMatches("/");
    command("cwd testdir-1", "250");
    command("cwd testdir-2", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2");
    command("cwd testdir-3", "250");
    command("cwd testdir-A", "450");
    command("cwd ..", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2");
    command("cwd ..", "250");
    command("cwd ..", "250");
    ensureWorkingDirectoryMatches("/");
    command("cwd testdir-1", "250");
    command("cwd testdir-2", "250");
    command("cwd testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd testdir-A", "450");
    command("cwd ../../..", "250");
    ensureWorkingDirectoryMatches("/");
    command("cwd testdir-1/testdir-2/testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd testdir-A", "450");
    command("cwd /", "250");
    command("cwd /testdir-1", "250");
    command("cwd /testdir-1/testdir-2", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2");
    command("cwd /testdir-1/testdir-2/testdir-3", "250");
    command("cwd /testdir-1/testdir-2/testdir-3/testdir-A", "450");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd /", "250");
    ensureWorkingDirectoryMatches("/");
    command("cwd testdir-1/testdir-2/testdir-3", "250");
    command("cwd /", "250");
    command("cwd testdir-1/testdir-2/testdir-3/testdir-A", "450");
    command("cwd /", "250");
    command("cwd ..", "250");
    ensureWorkingDirectoryMatches("/");
    command("cwd testdir-1", "250");
    command("cwd testdir-2", "250");
    command("cwd testdir-3", "250");
    command("cwd testdir-A", "450");
    command("cwd ../../..", "250");
    command("cwd testdir-1", "250");
    ensureWorkingDirectoryMatches("/testdir-1");
    command("cwd testdir-2", "250");
    command("cwd testdir-3", "250");
    command("cwd testdir-A", "450");
    command("cwd ../../testdir-2/testdir-3/../testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd /testdir-1/testdir-2/testdir-3/../testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd /../../../testdir-1", "250");
    ensureWorkingDirectoryMatches("/testdir-1");
    command("cwd testdir-2", "250");
    command("cwd ../testdir-2/testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd /testdir-A", "450");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd /", "250");
    command("cwd testdir-1/testdir-2", "250");
    command("cwd testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd ../testdir-3", "250");
    command("cwd ../../testdir-2/testdir-3", "250");
    command("cwd ../../testdir-2/testdir-3", "250");
    command("cwd ../../testdir-2/testdir-3/testdir-A", "450");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd ../../testdir-2/testdir-3/../testdir-3", "250");
    command("cwd ../../testdir-2/testdir-3/../testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd ../../testdir-2/testdir-3/../testdir-3/testdir-A", "450");
    command("cwd /testdir-1/testdir-2/testdir-3", "250");
    command("cdup", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2");
    command("cwd testdir-3", "250");
    command("cwd testdir-A", "450");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cdup", "250");
    command("cwd testdir-3/..", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2");
    command("cwd testdir-3", "250");
    command("cwd testdir-A", "450");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");

    logout();
  }


  public void testCreate() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-1/testdir-1.2", "257", "250");
    command("mkd testdir-1/testdir-1.2/testdir-1.3", "257", "250");
    command("cwd testdir-1/testdir-1.2/testdir-1.3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-1.2/testdir-1.3");

    command("cwd /", "250");
    command("mkd testdir-2", "257", "250");
    command("cwd testdir-2", "250");
    command("mkd testdir-2.2", "257", "250");
    command("cwd testdir-2.2", "250");
    command("mkd testdir-2.3", "257", "250");
    command("cwd testdir-2.3", "250");
    ensureWorkingDirectoryMatches("/testdir-2/testdir-2.2/testdir-2.3");

    command("cwd /", "250");
    command("mkd testdir-3", "257", "250");
    command("mkd testdir-3/testdir-3.2", "257", "250");
    command("cwd testdir-3/testdir-3.2", "250");
    command("mkd testdir-3.3", "257", "250");
    command("mkd testdir-3.3/testdir-3.4", "257", "250");
    command("cwd testdir-3.3/testdir-3.4", "250");
    ensureWorkingDirectoryMatches("/testdir-3/testdir-3.2/testdir-3.3/testdir-3.4");
    command("cwd /testdir-3/testdir-3.2/testdir-3.3/testdir-3.4", "250");

    command("cwd /", "250");
    command("mkd testdir-4", "257", "250");
    command("mkd testdir-4/testdir-4.2", "257", "250");
    command("mkd testdir-4/testdir-4.2/../testdir-4.3", "257", "250");
    command("mkd testdir-4/testdir-4.2/../testdir-4.4", "257", "250");
    command("cwd /testdir-4/testdir-4.2", "250");
    command("cwd /testdir-4/testdir-4.3", "250");
    command("cwd /testdir-4/testdir-4.4", "250");

    command("cwd /", "250");
    command("mkd testdir-5", "257", "250");
    command("mkd testdir-5/testdir-5.2/testdir-5.3", "450");
    command("cwd /testdir-5/testdir-5.2", "450");
    command("cwd /testdir-5/testdir-5.2/testdir-5.3", "450");
    command("mkd testdir-5/testdir-5.2", "257", "250");
    command("mkd testdir-5/testdir-5.2/testdir-5.3", "257", "250");
    command("cwd /testdir-5/testdir-5.2/testdir-5.3", "250");
    ensureWorkingDirectoryMatches("/testdir-5/testdir-5.2/testdir-5.3");

    command("cwd /", "250");
    command("mkd testdir-6 [bolt]", "257", "250");
    command("mkd testdir-6 [bolt]/testdir-6.2 [bolt]/testdir-6.3 [bolt]", "450");
    command("mkd testdir-6 [bolt]/testdir-6.2 [bolt]", "257", "250");
    command("mkd testdir-6 [bolt]/testdir-6.2 [bolt]/testdir-6.3 [bolt]", "257", "250");
    command("cwd /testdir-6 [bolt]/../testdir-6 [bolt]/testdir-6.2 [bolt]/../testdir-6.2 [bolt]/testdir-6.3 [bolt]", "250");
    ensureWorkingDirectoryMatches("/testdir-6 [bolt]/testdir-6.2 [bolt]/testdir-6.3 [bolt]");

    logout();
  }


  public void testBrowseOutside() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-2", "257", "250");

    command("cwd /", "250");
    ensureListedInWorkingDirectory("testdir-1", "testdir-2");
    ensureWorkingDirectoryMatches("/");

    command("cwd ..", "250");
    ensureListedInWorkingDirectory("testdir-1", "testdir-2");
    ensureWorkingDirectoryMatches("/");

    command("cwd /../..", "250");
    ensureListedInWorkingDirectory("testdir-1", "testdir-2");
    ensureWorkingDirectoryMatches("/");

    command("cwd /", "250");
    command("cdup", "250");
    ensureListedInWorkingDirectory("testdir-1", "testdir-2");
    ensureWorkingDirectoryMatches("/");

    command("cwd /testdir-1/../..", "250");
    ensureListedInWorkingDirectory("testdir-1", "testdir-2");
    ensureWorkingDirectoryMatches("/");

    logout();
  }


  public void testDelete() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-1/testdir-1.2", "257", "250");
    command("mkd testdir-1/testdir-1.2/testdir-1.3", "257", "250");
    command("cwd /testdir-1/testdir-1.2/testdir-1.3", "250");
    command("cwd /testdir-1/testdir-1.2/testdir-1.3/testdir-A", "450");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-1.2/testdir-1.3");

    command("rmd /testdir-1/testdir-1.2/testdir-1.3", "250");
    command("cwd /testdir-1/testdir-1.2", "250");
    command("cwd /testdir-1/testdir-1.2/testdir-1.3", "450");
    command("rmd /testdir-1/testdir-1.2", "250");
    command("cwd /testdir-1", "250");
    command("cwd /testdir-1/testdir-1.2", "450");
    command("rmd /testdir-1", "250");
    command("cwd /", "250");
    command("cwd /testdir-1", "450");
    ensureWorkingDirectoryMatches("/");

    command("mkd testdir-2", "257", "250");
    command("mkd testdir-2/testdir-2.2", "257", "250");
    command("mkd testdir-2/testdir-2.2/testdir-2.3", "257", "250");
    command("cwd /testdir-2/testdir-2.2/testdir-2.3", "250");
    command("cwd /testdir-2/testdir-2.2/testdir-2.3/testdir-A", "450");
    ensureWorkingDirectoryMatches("/testdir-2/testdir-2.2/testdir-2.3");

    command("cwd /testdir-2/testdir-2.2", "250");
    command("rmd testdir-2.3", "250");
    command("cwd testdir-2.3", "450");
    command("cwd /testdir-2", "250");
    command("rmd testdir-2.2", "250");
    command("cwd testdir-2.2", "450");
    command("cwd /", "250");
    command("rmd testdir-2", "250");
    command("cwd testdir-2", "450");
    ensureWorkingDirectoryMatches("/");

    command("mkd testdir-3", "257", "250");
    command("mkd testdir-3/testdir-3.2", "257", "250");
    command("mkd testdir-3/testdir-3.2/testdir-3.3", "257", "250");
    command("cwd /testdir-3/testdir-3.2/testdir-3.3", "250");
    command("cwd /testdir-3/testdir-3.2/testdir-3.3/testdir-A", "450");
    ensureWorkingDirectoryMatches("/testdir-3/testdir-3.2/testdir-3.3");

    command("rmd /testdir-3/testdir-3.2/../testdir-3.2/testdir-3.3", "250");
    command("cwd /testdir-3/testdir-3.2", "250");
    command("cwd /testdir-3/testdir-3.2/testdir-3.3", "450");
    command("cwd /testdir-3", "250");
    command("rmd /testdir-3/testdir-3.2/../testdir-3.2", "250");
    command("cwd /testdir-3", "250");
    command("cwd /testdir-3/testdir-3.2", "450");
    ensureWorkingDirectoryMatches("/testdir-3");

    logout();
  }


  public void testDeleteWorkingDir() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-1/testdir-2", "257", "250");
    command("mkd testdir-1/testdir-2/testdir-3", "257", "250");

    command("cwd /testdir-1/testdir-2/testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("rmd /testdir-1/testdir-2/testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("rmd /testdir-1/testdir-2", "250");
    command("cwd ..", "450");
    command("cdup", "450");
    command("cwd /testdir-1/testdir-2", "450");
    ensureWorkingDirectoryMatches("/testdir-1/testdir-2/testdir-3");
    command("cwd /", "250");
    command("cwd /testdir-1", "250");
    command("cwd /testdir-1/testdir-2", "450");
    ensureWorkingDirectoryMatches("/testdir-1");

    logout();
  }
}
