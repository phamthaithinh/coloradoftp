package com.coldcore.coloradoftp.testpack.test.genericbundle.special;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 *  Test if a server can handle rapid connections.
 */
public class FastReconnect extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(FastReconnect.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testSequence1() throws Exception {
    disconnectFtpClient();

    for (int z = 0; z < 5; z++) {
      connectFtpClient();
      anonymousLogin();
      logout();
    }

    for (int z = 0; z < 5; z++) {
      connectFtpClient();
      anonymousLogin();
      disconnectFtpClient();
    }

    connectFtpClient();
  }


  public void testSequence2() throws Exception {
    for (int z = 0; z < 5; z++) {
      disconnectFtpClient();
      connectFtpClient();
    }

    anonymousLogin();
    logout();
  }

}
