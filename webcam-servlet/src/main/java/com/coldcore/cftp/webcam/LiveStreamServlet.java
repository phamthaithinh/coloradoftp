package com.coldcore.cftp.webcam;

import com.coldcore.misc5.CByte;
import com.coldcore.gfx.Gfx;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.awt.*;

public class LiveStreamServlet extends HttpServlet {

  private static Logger log = Logger.getLogger(LiveStreamServlet.class);
  private Map<String,byte[]> channels;
  private Map<String,Long> posted;
  private String password;
  private int timeout;
  private int maxsize;


  public void init(ServletConfig servletConfig) throws ServletException {
    super.init(servletConfig);
    channels = new HashMap<String,byte[]>();
    posted = new HashMap<String,Long>();
    password = servletConfig.getInitParameter("upload-password");
    timeout = 300;
    try {
      timeout = Integer.parseInt(servletConfig.getInitParameter("channel-timeout"));
    } catch (Throwable e) {}
    maxsize = 500;
    try {
      maxsize = Integer.parseInt(servletConfig.getInitParameter("picture-size"));
    } catch (Throwable e) {}
    log.info("Initialized: timeout "+timeout+" seconds, max size "+maxsize+" KB");
  }


  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      String channel = request.getParameter("channel");
      if (channel == null) channel = "";
      byte[] data = channels.get(channel);
      Long time = posted.get(channel);
      if (time == null || time+timeout*1000L < System.currentTimeMillis()) data = null; //Channel timeout, wait the next POST
      prepareDownload(data, response);
    } catch (Exception e) {
      throw new ServletException(e);
    }
  }


  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      saveIncomingPicture(request);
    } catch (Exception e) {
      throw new ServletException(e);
    }
  }


  private void saveIncomingPicture(HttpServletRequest request) throws Exception {
    ServletFileUpload sfu = new ServletFileUpload(new DiskFileItemFactory());
    List<FileItem> fileItems = sfu.parseRequest(request);

    String channel = null;
    String pass = null;
    byte[] data = null;

    for (FileItem fi : fileItems) {

      if (fi.getFieldName().equals("channel")) {
        channel = fi.getString();
      }

      if (fi.getFieldName().equals("password")) {
        pass = fi.getString();
      }

      if (fi.getFieldName().equals("picture")) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        CByte.inputToOutput(Channels.newChannel(fi.getInputStream()), Channels.newChannel(out));
        data = out.toByteArray();
      }

    }

    if (!password.equals(pass)) {
      log.warn("Unauthorized upload attempt");
      return;
    }

    Image img = Gfx.createImage(data);
    if (!Gfx.isValid(img)) {
      log.warn("Not a valid picture file");
      return;
    }

    if (data.length > maxsize*1024) {
      log.warn("Picture is too big");
      return;
    }

    if (channel != null && data != null) {
      channels.put(channel, data);
      posted.put(channel, System.currentTimeMillis());
    }
  }


  private void prepareDownload(byte[] data, HttpServletResponse response) throws Exception {
    if (data == null) {
      response.setStatus(HttpServletResponse.SC_NOT_FOUND);
      return;
    }

    //No cache
    response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");

    response.setContentType("image/jpeg");
    response.setHeader("Content-Disposition", "inline; filename=\"livestream.jpg\"");
    response.setContentLength(data.length);

    if (data.length > 0) {
      response.resetBuffer();
      OutputStream out = response.getOutputStream();
      CByte.inputToOutput(new ByteArrayInputStream(data), out);
      response.flushBuffer();
    }
  }

}

